import java.util.HashMap;

public class Node {
    private HashMap<Character, Integer> nexts;
    private char endchar;
    private char[] alphabet;
    private Character[] outputs;

    public Node(Integer[] nexts, Character[] outputs, char[] alpha)
    {
        this.nexts = new HashMap<>();
        for (int i=0; i < alpha.length; i++) {
            this.nexts.put(alpha[i], nexts[i]);
        }
        this.endchar = '-';
        this.alphabet = alpha;
        this.outputs = outputs;
    }

    public Node(char[] symbols, int[] nexts, char[] alpha)
    {
        this.nexts = new HashMap<>();
        this.endchar = '-';
        this.alphabet = alpha;
        this.outputs = new Character[nexts.length];

        for (int j = 0; j < outputs.length; j++) {
            outputs[j] = '0';
        }

        for (int i = 0; i < symbols.length; i++) {
            this.nexts.put(symbols[i], nexts[i]);
        }
    }

    public Node(char[] symbols, int[] nexts, char endchar, char[] alpha)
    {
        this.nexts = new HashMap<>();
        this.endchar = endchar;
        this.alphabet = alpha;
        this.outputs = new Character[nexts.length];

        for (int j = 0; j < outputs.length; j++) {
            outputs[j] = '0';
            if (alpha[j] == endchar) outputs[j] = '1';
        }

        for (int i = 0; i < symbols.length; i++) {
            this.nexts.put(symbols[i], nexts[i]);
        }
    }

    public int getNext(char symbol)
    {
        return nexts.get(symbol);
    }

    public char getOutput(char symbol)
    {
        char out = '-';
        for (int i = 0; i < alphabet.length; i++) {
            if (alphabet[i] == symbol) out = outputs[i];
        }
        return out;
    }

    public void setNexts(HashMap<Character, Integer> nexts) {
        this.nexts = nexts;
    }

    public HashMap<Character, Integer> getNexts() {
        return nexts;
    }

    @Override
    public String toString() {
        return nexts.toString();
    }
}
