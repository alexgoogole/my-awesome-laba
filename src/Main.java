public class Main {

    public static void main(String[] args) {
        char[] symbols = {'x', 'y'};
        String inputWord = "xyxyxy";
        String testWord = "xyxyxyxyxyxyxyxy";
        //Machine myMach = new Machine(symbols, inputWord);
        Integer[][] next = {
                {2,5},
                {3,7},
                {0,3},
                {6,8},
                {8,0},
                {2,4},
                {3,2},
                {3,1},
                {4,6},
        };
        Character[][] outputs = {
                {'1','0'},
                {'0','1'},
                {'1','0'},
                {'0','1'},
                {'1','0'},
                {'1','0'},
                {'0','1'},
                {'1','0'},
                {'1','0'},
        };
        Machine myMach = new Machine(symbols, next, outputs);

        System.out.print(myMach);
        System.out.print(myMach.run(testWord) + "\n");
        String ans = myMach.run(testWord);
        myMach.minimize();
        System.out.print(ans + "\n");
    }
}
