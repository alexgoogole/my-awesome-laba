import java.util.ArrayList;
import java.util.HashMap;

public class Machine {
    Node[] algo;
    char[] alphabet;
    public Machine(char[] symbols, String word) {
        alphabet = symbols;
        int symbolsCount = symbols.length;
        char[] wordSymb = word.toCharArray();

        //определяем длину КА
        algo = new Node[wordSymb.length];
        //заполнение
        for (int i = 0; i < wordSymb.length; i++) {
            int[] nexts = new int[symbolsCount];

            //строим состояния алгоритма для каждого из возможных вариантов символа
            for (int j = 0; j < symbolsCount; j++) {

                // если увидим верный символ и это не последняя итерация,
                // то просто ставим переход в следующее состояние
                if (symbols[j] == wordSymb[i] && i + 1 < wordSymb.length) {
                    nexts[j] = i+1;
                } else {
                    //вычисляем в какое состояние КА будет возвращаться
                    //слово, которое мы "увидим" на входе в этом случае
                    String wordToSee = word.substring(0, i) + symbols[j];
                    int offset = 1;

                    // "отступаем" от слова, которое мы видим на входе,
                    // по 1 символу слева и смотрим, не совпадает ли оно с началом слова.
                    while (offset < wordToSee.length()) {
                        String str1 = wordToSee.substring(offset);
                        String str2 = word.substring(0, wordToSee.length() - offset);
                        if (str1.compareTo(str2) == 0) {
                            break;
                        }
                        offset++;
                    }
                    nexts[j] = wordToSee.length() - offset;
                }
            }
            if (i + 1 < word.length()) {
                algo[i] = new Node(symbols, nexts, alphabet);
            } else {
                algo[i] = new Node(symbols, nexts, word.charAt(word.length() - 1), alphabet);
            }

        }
    }

    public Machine(char[] symbols, Integer[][] states, Character[][] outs) {
        alphabet = symbols;
        int symbolsCount = symbols.length;


        //определяем длину КА
        algo = new Node[states.length];
        //заполнение
        for (int i = 0; i < algo.length; i++) {
            algo[i] = new Node(states[i], outs[i], symbols);
        }
    }

    public String run(String word)
    {
        char[] wordSymb = word.toCharArray();
        int next = 0;
        String output = "";
        for (char symbol : wordSymb) {
            output += algo[next].getOutput(symbol);
            next = algo[next].getNext(symbol);
        }
        return output;
    }

    public void minimize()
    {
        HashMap<Character, ArrayList<Integer>> equalityClasses = new HashMap<>();
        HashMap<Character, ArrayList<Character>> equalities = new HashMap<>();

        int i = 0;

        // перебираем алгоритм и разбиваем на первичные классы эквивалентности
        for (int j = 0; j < algo.length; j++) {

            //строим строчку с выводами
            ArrayList<Character> outputs = new ArrayList<>(alphabet.length);
            for (char alpha: alphabet) {
                outputs.add(algo[j].getOutput(alpha));
            }

            //если не было этого класса эквивалентности - то добавляем в группу классов эквивалентности и добавляем первый элемент в класс
            if (!equalities.containsValue(outputs)) {
                equalities.put((char)(65 + i), outputs);
                ArrayList<Integer> start = new ArrayList<>();
                start.add(j);
                equalityClasses.put((char)(65 + i), start);
                i++;
            } else {
                for (HashMap.Entry<Character, ArrayList<Character>> oneC :equalities.entrySet()) {
                    int eq = 0;
                    for (int t = 0; t < oneC.getValue().size(); t++) {
                        if (oneC.getValue().get(t) == outputs.get(t)) eq++;
                    }

                    if (eq == alphabet.length) {
                        equalityClasses.get(oneC.getKey()).add(j);
                    }
                }
            }
        }
        System.out.print("Minimisation:\n0-equal classes:\n");
        System.out.print(equalityClasses + "\n");

        // теперь продолжаем деление на классы эквивалентности
        // пока количество классов не сойдется с количеством на предыдущей итерации

        int countClasses = equalities.size();
        int newCountClasses = equalities.size();
        int iteration = 0;
         do {
             countClasses = newCountClasses;
             //копируем  старые классы и типы, чистим текущие
             HashMap<Character, ArrayList<Integer>> oldClasses = new HashMap<>(equalityClasses);
             equalityClasses.clear();
             equalities.clear();
             int dividers = 0;
             //перебираем все старые классы эквивалентности
             for (HashMap.Entry<Character, ArrayList<Integer>> currClass : oldClasses.entrySet())
             {

                 // типы эквивалентности для этого класса эквивалентности
                 HashMap<Character, ArrayList<Character>> currEq = new HashMap<>();
                 // для каждого состояния в этом классе собираем вывод
                 for (Integer stateNum: currClass.getValue()) {
                     // переменная для вывода
                     ArrayList<Character> equalityType = new ArrayList<>();
                     for (char alpha: alphabet) {
                         //строим новую строчку с выводами
                         int output = algo[stateNum].getNext(alpha);
                         char eqOut = 'q';
                         for (int k = 0; k < oldClasses.size(); k++) {
                             if (oldClasses.get((char)(65 + k)).contains(output)) eqOut = (char)(65 + k);
                         }
                         equalityType.add(eqOut);
                     }
                     //equalityType - то, к каким классам относятся выводы, теперь разделяем этот класс если надо.
                    if (! currEq.containsValue(equalityType)) {
                        currEq.put((char)(65 + dividers), equalityType);
                        ArrayList<Integer> first = new ArrayList<>();
                        first.add(stateNum);
                        equalityClasses.put((char)(65 + dividers), first);
                        dividers++;
                    } else {
                        for (HashMap.Entry<Character, ArrayList<Character>> oneC :currEq.entrySet()) {
                            int eq = 0;
                            for (int t = 0; t < oneC.getValue().size(); t++) {
                                if (oneC.getValue().get(t) == equalityType.get(t)) eq++;
                            }

                            if (eq == alphabet.length) {
                                equalityClasses.get(oneC.getKey()).add(stateNum);
                            }
                        }
                    }

                 }
                 equalities.putAll(currEq);
                 currEq.clear();
             }
             iteration++;
             System.out.print(iteration + "-equal classes:\n");
             System.out.print(equalityClasses.toString() + "\n");
             System.out.print(equalities.toString() + "\n");

             newCountClasses = equalityClasses.size();
        } while (countClasses < newCountClasses && newCountClasses < algo.length);

        ArrayList<Node> minimized = new ArrayList<>();
        for (HashMap.Entry<Character, ArrayList<Integer>> entityClass : equalityClasses.entrySet()) {
            Node n = algo[entityClass.getValue().get(0)];
            HashMap<Character, Integer> newAlgoNode = n.getNexts();
            for (HashMap.Entry<Character, Integer> node : newAlgoNode.entrySet()) {
                Integer out = node.getValue();
                for (HashMap.Entry<Character, ArrayList<Integer>> currClass: equalityClasses.entrySet()) {
                    if (currClass.getValue().contains(out)) node.setValue(currClass.getValue().get(0));
                }
            }
            n.setNexts(newAlgoNode);
            minimized.add(n);
        }
        algo = minimized.toArray(new Node[minimized.size()]);
        System.out.println("Minimized machine");
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        String out = "";
        for (int k = 0; k < algo.length; k++) {
            out+= k + ": " + algo[k].toString() + " [ ";
            for (char symb: alphabet) {
                out += algo[k].getOutput(symb) + " ";
            }
            out += "]\n";
        }
        return out;
    }
}
